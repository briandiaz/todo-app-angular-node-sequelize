# How To Run App

In order to run this app you must type in your terminal the next commands (one by one):

Download postgresql packages:
```{r, engine='bash'}
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" /etc/apt/sources.list.d/pgdg.list'
wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -
```
Update your system:
```{r, engine='sh', count_lines}
sudo apt-get update
```
Install postgresql:
```{r, engine='sh', count_lines}
sudo apt-get install postgresql postgresql-contrib
```
Sign in as postgres. 
```{r, engine='sh', count_lines}
sudo su - postgres
```
Run postgres:
```{r, engine='sh', count_lines}
psql
```
Create admin role:
```{r, engine='sh', count_lines}
CREATE ROLE admin LOGIN PASSWORD 'admin';
```
Create todos database:
```{r, engine='sh', count_lines}
CREATE DATABASE todos OWNER admin;
```
Connect to todos database:
```{r, engine='sh', count_lines}
\c todos
```
Create todos table:
```{r, engine='sh', count_lines}
CREATE TABLE todos (id SERIAL PRIMARY KEY, text name, done boolean);
```

*This is by default, but to make sure it is as instructed.*

```{r, engine='sh', count_lines}
ALTER SEQUENCE todos_id_seq   start 1  increment 1;
```
Set all permissions to admin role:
```{r, engine='sh', count_lines}
GRANT ALL PRIVILEGES ON TABLE todos TO admin;
```
Set all permissions to serial id sequence to admin:
```{r, engine='sh', count_lines}
GRANT ALL PRIVILEGES ON SEQUENCE todos_id_seq TO admin;
```
Exit postgress and install dependencies:
```{r, engine='sh', count_lines}
npm install
```
```{r, engine='sh', count_lines}
bower install
```
Run project:
```{r, engine='sh', count_lines}
npm install
```