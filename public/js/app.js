'use strict';

/*================================================
Module - Main App Module
================================================ */
angular.module('postgreDbApp', ['ngRoute', 'postgreDbApp.controllers', 'postgreDbApp.services', 'ui.sortable'])


.config(function ($routeProvider, $locationProvider) {

  /*================================================
  Define all the Routes
  Ref.
  https://docs.angularjs.org/api/ng/provider/$locationProvider
  ================================================ */
	$routeProvider
    
    .when('/', {
        templateUrl: 'views/main.tpl.html',
        controller: 'MainCtrl',
        reloadOnSearch: false,
        resolve: {
          logincheck: isLoggedIn
        }
    })
    .when('/login', {
        templateUrl: 'views/login.tpl.html',
        controller: 'LoginController',
        reloadOnSearch: false
    })
    .when('/register', {
        templateUrl: 'views/register.tpl.html',
        controller: 'RegisterController',
        reloadOnSearch: false
    })
    
    .otherwise({
        redirectTo: '/'
    });


    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });

    function isLoggedIn($q, $timeout, $http, $location, $rootScope) {
        var deferred = $q.defer();

        $http.get('/api/sessions')
            .success(function(user) {
              $rootScope.currentUser = user;
              deferred.resolve();
            })
            .error(function(error) {
              deferred.reject();
              $location.url('/login');
            });
        return deferred.promise;
    }
  });

