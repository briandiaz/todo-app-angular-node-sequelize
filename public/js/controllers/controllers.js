'use strict';

/*================================================
Module - for the Controllers
================================================ */
angular.module('postgreDbApp.controllers', [])

/**
 * Controller - MainCtrl
 */
.controller('MainCtrl', function($scope, $q, todoService) {

	$scope.formData = {};
	$scope.todos={};

	todoService.getAll()
		.then(function(answer) {
			$scope.todos = answer;
		},
		function(error) {
			console.log("OOPS!!!! " + JSON.stringify(error));
		}
  	);

	$scope.boardPanel = {
		connectWith: '.list-group',
		stop: function(e, ui) {
			var draggedTodo = ui.item.scope().todo;
			draggedTodo.done = !draggedTodo.done;

			todoService.update(draggedTodo.id, draggedTodo)
				.then(function(answer) {
					$scope.todos = answer;
				},
				function(error) {
					console.log("OOPS Error Updating!!!! " + JSON.stringify(error));
				}
			);
		}
	};

	/*
	 * Create a New Todo
	 */
	$scope.createTodo = function() {
		todoService.create($scope.formData)
			.then(function(answer) {
				$scope.todos = answer;
				$scope.formData.text = '';
			},
			function(error) {
				console.log("OOPS Error Creating Todo!!!! " + JSON.stringify(error));
			}
	  	);
	};


	/*
	 * Update a Todo
	 */
	$scope.editTodo = function(id, txt, isDone) {

		var updateData = {"text":txt, "done": isDone};

		todoService.update(id, updateData)
			.then(function(answer) {
				$scope.todos = answer;
			},
			function(error) {
				console.log("OOPS Error Updating!!!! " + JSON.stringify(error));
			}
	  	);
	};


	/*
	 * Delete a Todo
	 */
	$scope.deleteTodo = function(id) 
	{
		todoService.delete(id)
			.then(function(answer) {
				$scope.todos = answer;
			},
			function(error) {
				console.log("OOPS Error Deleting!!!! " + JSON.stringify(error));
			}
	  	);

	};
})


.controller("LoginController", function($location, $scope, $http, $rootScope) {
	$rootScope.flashMessage = '';
	$scope.user = {};

	$scope.login = function() {
		$http.post('/api/sessions/login', $scope.user)
			.success(function(response) {
				$rootScope.currentUser = response;
				$rootScope.flashMessage = 'Signed in successfully!';
				$rootScope.isError = false;
				$location.url("/");
			})
			.catch(function (error) {
				$rootScope.flashMessage = 'Wrong Username or Password. Please try again!';
				$rootScope.isError = true;
			});
	}
})

.controller("RegisterController", function($location, $scope, $http, $rootScope) {
	$rootScope.flashMessage = '';
	$scope.user = {};

	if ($rootScope.currentUser) {
		$location.url("/");
	}

	$scope.register = function() {
		$http.post('/api/users/', $scope.user)
			.success(function(response) {
				$rootScope.flashMessage = 'Registered successfully!';
				$rootScope.isError = false;
				$location.url("/login");
			})
			.catch(function (error) {
				$rootScope.flashMessage = 'There was an error trying to register. Please try again!';
				$rootScope.isError = true;
			});
	}
})


.controller("MenuController", function($location, $scope, $http, $rootScope) {
	$rootScope.flashMessage = '';

	$scope.logout = function() {
		$http.post("/api/sessions/logout")
			.success(function() {
				$rootScope.currentUser = null;
				$rootScope.isError = false;
				$location.url("/login");
			})
			.catch(function (error) {
				$rootScope.flashMessage = 'There was an error trying to log out your session. Please try again!';
				$rootScope.isError = true;
			});
	}
});