/*================================================
Ref.
https://docs.angularjs.org/api/ng/service/$q
https://github.com/kriskowal/q
http://www.benlesh.com/2013/02/angularjs-creating-service-with-http.html
http://andyshora.com/promises-angularjs-explained-as-cartoon.html
================================================ */

'use strict';
/*================================================
Module - for the Services
================================================ */
angular.module('postgreDbApp.services', [])

/**
 * getTodos - Factory Service
 */
.factory('todoService', function($http, $q) {

	/*================================================================
	READ - $http get
	=================================================================*/
    var deferred = $q.defer();

    //Return Factory Object
    return {
        getAll() {
            deferred = $q.defer();
            $http.get('/api/todos/')
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(reason) {
                    deferred.reject(reason);
                });
            return deferred.promise;
        },
        create(data) {
            deferred = $q.defer();
            $http.post('/api/todos/', data)
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(reason) {
                    deferred.reject(reason);
                });
            return deferred.promise;
        },
        update(id, data) {
            deferred = $q.defer();
            $http.put('/api/todos/' + id, data)
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(reason) {      	
                    deferred.reject(reason);
                });
            return deferred.promise;
        },
        delete(id) {
            deferred = $q.defer();
            $http.delete('/api/todos/' + id)        
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(reason) {
                    deferred.reject(reason);
                });
            return deferred.promise;
        }
    };
})
