/*=========================================================
Michael Cullen
Todo CRUD - Node / Express / Angular / PostgreSQL
server.js

2014

Working - (Tá se ag obair)

Ref.
http://stackoverflow.com/questions/8484404/what-is-the-proper-way-to-use-the-node-js-postgresql-module
https://gist.github.com/brianc/6908287
http://stackoverflow.com/questions/15619456/how-do-i-use-node-postgres-in-a-server

============================================================*/

/* ========================================================== 
External Modules/Packages Required
============================================================ */
const express  = require('express');								//Express
const logger   = require('morgan');								//logger middleware
const bodyParser = require('body-parser');						//needed to read HTTP packet content using req.body etc
const path = require('path');
const http = require('http');
const colours = require('colors');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const Sequelize = require('./server/mapper/sequelize');
const errorService = require('./server/middlewares/error-handler');

/* ========================================================== 
Internal App Modules/Packages Required
============================================================ */
const routes = require('./server/routes');						//Exchange routes & DB Queries 
const passport = require('passport');
const passportConfig = require('./server/config/passport');

/* ========================================================== 
Create a new application with Express
============================================================ */
const app = express();

const mapper = new Sequelize();

const apiPrefix = process.env.PREFIX || '/api';

/* ========================================================== 
Set the Port the HTTP server will listen on
============================================================ */
app.set('port', process.env.PORT || 3090);							

/* ========================================================== 
serve the static index.html from the public folder
============================================================ */
app.use(express.static(__dirname + '/public')); 


passportConfig(passport, mapper);

app.use(session({
  secret: 'secret-todoapp',
  resave: true,
  saveUninitialized: true
}));
app.use(cookieParser());

app.use(passport.initialize());
app.use(passport.session());

/* ========================================================== 
Use Middleware
============================================================ */
app.use(logger('dev')); 	//log every request to the console
		
// parse application/json
app.use(bodyParser.json()) //Get info from $HTTP POST/PUT packets - needed for req.body

/* ========================================================== 
ROUTES - using Express
============================================================ */
app.use(apiPrefix, routes({ mapper, errorService, passport }));

/* ========================================================== 
Create HTTP Server using Express
============================================================ */
const server = http.createServer(app);

/* ========================================================== 
Bind to a port and listen for connections on it 
============================================================ */
server.listen(app.get('port'), function() {
  console.log('Express HTTP server listening on port ' .red + app.get('port') ) ;
});

