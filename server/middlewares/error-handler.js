
module.exports = {
    assertRequest({ passes, onFailureThrow: error, status, response  }) {
        if (!passes) {
            const err = new Error(error); 
            this.showError(err, status, response);
            throw err;
        }
    },
    showError(error, status, response) {
        response
            .status(status)
            .send({ status, error: error.message });
    },
};
