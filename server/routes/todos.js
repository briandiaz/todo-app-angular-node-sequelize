const TodoService = require('../services/todos.js');
const express = require('express');


module.exports = function createTodoRoutes({ mapper, errorService }) {
	const todos = TodoService({ mapper, errorService });
    const router = express.Router();
	/*================================================================
	ROUTES
	=================================================================*/
	router
		.post('/', todos.create)

		.get('/', todos.read)

		.put('/:id', todos.update)

		.delete('/:id', todos.delete);

	return router;
};