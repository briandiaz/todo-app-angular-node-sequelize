const SessionService = require('../services/sessions.js');
const express = require('express');


module.exports = function createSessionsRoutes({ mapper, errorService, passport }) {
	const sessions = SessionService({ mapper, errorService });
    const router = express.Router();
	/*================================================================
	ROUTES
	=================================================================*/
	router
		.get('/', sessions.getCurrentSession)

		.post('/login', passport.authenticate('local-login'), sessions.login)

		.post('/logout', sessions.logout);

	return router;
};