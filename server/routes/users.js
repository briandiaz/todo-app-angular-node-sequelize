const UserService = require('../services/users.js');
const express = require('express');


module.exports = function createUserRoutes({ mapper, errorService }) {
	const users = UserService({ mapper, errorService });
    const router = express.Router();
	/*================================================================
	ROUTES
	=================================================================*/
	router
		.post('/', users.create)

		.get('/:username', users.profile)

		.put('/:id', users.update);

	return router;
};