/*================================================================
	Server side Routing
	Route Declarations

=================================================================*/

/* ========================================================== 
Internal App Modules/Packages Required
============================================================ */
const express = require('express');
const TodoRoutes = require('./routes/todos.js');
const UserRoutes = require('./routes/users.js');
const SessionRoutes = require('./routes/sessions.js');


module.exports = function({ mapper, errorService, passport }) {
	const router = express.Router();
	
	router.use('/todos', TodoRoutes({ mapper, errorService }));
	router.use('/users', UserRoutes({ mapper, errorService }));
	router.use('/sessions', SessionRoutes({ mapper, errorService, passport }));

	return router;
};