module.exports = {
    MESSAGE: {
        SUCCESSFUL_CONNECTION: 'Connection has been established successfully.', 
    },
    ERRORS: {
        WRONG_CONNECTION: 'Unable to connect to the database:',
    },
}