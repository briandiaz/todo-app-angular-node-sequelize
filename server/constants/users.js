module.exports = {
    MESSAGE: {
    },
    ERRORS: {
        ID_PARAMETER_MISSING: 'Parameter \'id\' is missing.',
        EMAIL_PARAMETER_MISSING: 'Parameter \'email\' is missing.',
        USERNAME_PARAMETER_MISSING: 'Parameter \'username\' is missing.',
        PASSWORD_PARAMETER_MISSING: 'Parameter \'password\' is missing.',
        NO_DATA: 'Payload is empty.',
        USER_NOT_FOUND: 'User not found.',
    },
}