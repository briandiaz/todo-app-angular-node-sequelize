module.exports = {
    MESSAGE: {
    },
    ERRORS: {
        ID_PARAMETER_MISSING: 'Parameter \'id\' is missing.',
        TEXT_PARAMETER_MISSING: 'Parameter \'text\' is missing.',
        DONE_PARAMETER_MISSING: 'Parameter \'done\' is missing.',
        NO_DATA: 'Payload is empty.',
    },
}