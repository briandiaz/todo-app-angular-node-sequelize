const HTTP_STATUS = require('http-status');

module.exports = function SessionService({ mapper, errorService }) {

	return {
		login(request, response) {
            response.json(request.user);
		},

		logout(request, response) {
            request.logOut();
            const status = 200;
            response.status(status);
            response.json({ status, message: 'Logged out.'});
		},
        getCurrentSession(request, response) {
            const isAuthenticated = request.isAuthenticated();
            const status = isAuthenticated ? HTTP_STATUS.OK : HTTP_STATUS.NOT_FOUND;
            const data = isAuthenticated ? request.user : {
                status,
                message: 'No session found.',
            }
            response.status(status);
            response.json(data);
        },
	};
}
