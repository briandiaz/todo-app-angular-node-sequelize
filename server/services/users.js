const UserModel = require('../models/users');
const HTTP_STATUS = require('http-status');
const CONSTANTS = require('../constants/users.js');

module.exports = function UserService({ mapper, errorService }) {
	const Users = new UserModel(mapper);

    const findUserBy = (username, response, status = 200) => {
        Users
            .findOne({
                where: { username },
                attributes: ['id', 'username', 'email'],
            })
            .then((user) => {
                if (!user) {
                    response.status(HTTP_STATUS.NOT_FOUND);
                    response.send({ status: HTTP_STATUS.NOT_FOUND, message: CONSTANTS.ERRORS.USER_NOT_FOUND });
                } else {
                    response.status(status);
                    response.json(user);
                }
            })
            .catch((error) => {
                errorService.showError(error, HTTP_STATUS.INTERNAL_SERVER_ERROR, response)
            });
    };

	return {
		create(request, response) {
			const { body } = request;

			errorService.assertRequest({
				passes: body.email,
				onFailureThrow: CONSTANTS.ERRORS.EMAIL_PARAMETER_MISSING,
				status: HTTP_STATUS.BAD_REQUEST,
				response,
			});

			errorService.assertRequest({
				passes: body.username,
				onFailureThrow: CONSTANTS.ERRORS.USERNAME_PARAMETER_MISSING,
				status: HTTP_STATUS.BAD_REQUEST,
				response,
			});

			errorService.assertRequest({
				passes: body.password,
				onFailureThrow: CONSTANTS.ERRORS.PASSWORD_PARAMETER_MISSING,
				status: HTTP_STATUS.BAD_REQUEST,
				response,
			});

			const data = {
                email: body.email,
                username: body.username,
                password: body.password,
			};

			Users
				.create(data)
				.then(() => {
                    findUserBy(data.username, response, HTTP_STATUS.CREATED);
				})
                .catch((error) => {
                    errorService.showError(error, HTTP_STATUS.INTERNAL_SERVER_ERROR, response)
                });
		},

		profile(request, response) {
            const { username } = request.params;
			findUserBy(username, response);
		},

		update(request, response) {
			const { id } = request.params;

			errorService.assertRequest({
				passes: id,
				onFailureThrow: CONSTANTS.ERRORS.ID_PARAMETER_MISSING,
				status: HTTP_STATUS.BAD_REQUEST,
				response,
			});

			const { body } = request;

			errorService.assertRequest({
				passes: body,
				onFailureThrow: CONSTANTS.ERRORS.NO_DATA,
				status: HTTP_STATUS.BAD_REQUEST,
				response,
			});

			const { email, username, password } = body;

			Users
				.update({
                    email,
                    username,
                    password
                }, {
					where: { id	},
				})
				.then(() => {
                    findUserBy(username, response, HTTP_STATUS.ACCEPTED);
				})
                .catch((error) => {
                    errorService.showError(error, HTTP_STATUS.INTERNAL_SERVER_ERROR, response)
                });
			

		},

        // To be implemented
		delete(request, response) {},
	};
}
