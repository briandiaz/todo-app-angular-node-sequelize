const TodoModel = require('../models/todos');
const HTTP_STATUS = require('http-status');
const CONSTANTS = require('../constants/todos.js');

module.exports = function TodoService({ mapper, errorService }) {
	const Todos = new TodoModel(mapper);

	const findAll = (response, status = 200) => {
		Todos
			.findAll({
				order: [[ 'id', 'ASC' ]]
			})
			.then((todos) => {
				response.status(status)
				return response.json(todos);
			})
            .catch((error) => {
                errorService.showError(error, HTTP_STATUS.INTERNAL_SERVER_ERROR, response)
            });
	};

	return {
		create(request, response) {
			const { body } = request;

			errorService.assertRequest({
				passes: body.text,
				onFailureThrow: CONSTANTS.ERRORS.TEXT_PARAMETER_MISSING,
				status: HTTP_STATUS.BAD_REQUEST,
				response,
			});

			const data = {
				text: body.text,
				done: false
			};

			Todos
				.create(data)
				.then(() => {
					findAll(response, HTTP_STATUS.CREATED);
				})
				.catch((error) => {
					errorService.showError(error, HTTP_STATUS.INTERNAL_SERVER_ERROR, response)
				});
		},

		read(request, response) {
			findAll(response);
		},

		update(request, response) {
			const { id } = request.params;

			errorService.assertRequest({
				passes: id,
				onFailureThrow: CONSTANTS.ERRORS.ID_PARAMETER_MISSING,
				status: HTTP_STATUS.BAD_REQUEST,
				response,
			});

			const { body } = request;

			errorService.assertRequest({
				passes: body,
				onFailureThrow: CONSTANTS.ERRORS.NO_DATA,
				status: HTTP_STATUS.BAD_REQUEST,
				response,
			});

			const { text, done } = body;

			Todos
				.update({
					text,
					done,
				}, {
					where: {
						id,
					},
				})
				.then(() => {
					findAll(response, HTTP_STATUS.ACCEPTED);
				})
				.catch((error) => {
					errorService.showError(error, HTTP_STATUS.INTERNAL_SERVER_ERROR, response)
				});
			

		},

		delete(request, response) {
			const { id } = request.params;

			errorService.assertRequest({
				passes: id,
				onFailureThrow: CONSTANTS.ERRORS.ID_PARAMETER_MISSING,
				status: HTTP_STATUS.BAD_REQUEST,
				response,
			});

			Todos
				.destroy({
					where: {
						id,
					},
				})
				.then(() => {
					findAll(response);
				})
				.catch((error) => {
					errorService.showError(error, HTTP_STATUS.INTERNAL_SERVER_ERROR, response)
				});
		},
	};
}
