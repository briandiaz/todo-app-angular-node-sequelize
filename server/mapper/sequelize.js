const CONSTANTS = require('../constants/db');
const { connectionString } = require('../config/database');
const Sequelize = require('sequelize');

module.exports = function createMapperService() {
    const mapper = new Sequelize(connectionString);

    mapper
        .authenticate()
        .then(() => console.log(CONSTANTS.MESSAGE.SUCCESSFUL_CONNECTION.green))
        .catch(err => console.log(CONSTANTS.ERRORS.WRONG_CONNECTION.red, err));

    return mapper;
}
