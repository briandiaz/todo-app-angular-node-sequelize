const Sequelize = require('sequelize');

module.exports = function (mapper) {
    const Todo = mapper.define('todos', {
        text: {
            type: Sequelize.STRING, // Should be NAME type at database
        },
        done: {
            type: Sequelize.BOOLEAN,
        },
    }, {
      timestamps: false,
    });

    Todo.sync();

    return Todo;
};
