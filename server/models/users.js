const Sequelize = require('sequelize');

module.exports = function UserModel(mapper) {
    const User = mapper.define('user', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
        },
        username: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
        },
    }, {
      timestamps: false,
    });

    User.sync();
    
    User
        .findOrCreate({
            where: {
                username: 'admin'
            },
            defaults: {
                email: 'briandiaz@outlook.com',
                username: 'admin',
                password: 'admin',
            },
        });

    return User;
};
