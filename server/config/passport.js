const LocalStrategy = require('passport-local').Strategy;
const UserModel = require('../models/users');

module.exports = function passportConfig(passport, mapper) {
	const Users = new UserModel(mapper);
    function passportStrategy(username, password, done) {
        console.log(username, password);
        Users
            .findOne({
                where: { username: username.toLowerCase() },
            })
            .then((user) => {
                if (!user) return done(null, false);

                if (user.password !== password) return done(null, false);

                console.log('logged in'.green);

                done(null, user);
            })
            .catch((error) => {
                done(error);
            });
    }

    passport.serializeUser((user, done) => {
        done(null, user);
    });

    passport.deserializeUser((user, done) => {
        done(null, user);
    });


    passport.use('local-login', new LocalStrategy(passportStrategy));

};